import time
import os
import numpy as np
import sys
import tensorflow as tf
#from PIL import Image
import astar
# This is needed since the notebook is stored in the object_detection folder.
sys.path.append("..")
from object_detection.utils import ops as utils_ops
from utils import label_map_util
import cv2
def get_new_box(left,right,top,bottom):
    hor = right - left
    ver = bottom - top
    left = left - int(hor/12)
    right = right  + int(hor/12)
    top = top - int(ver/5)
    bottom = bottom  + int(hor/4.5)
    return left,right,top,bottom
    
def calc_path(frame,dataLST): # 1-white,0-black,2-center of minion,
    grayImage = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    (thresh, blackAndWhiteImage) = cv2.threshold(grayImage, 127, 255, cv2.THRESH_BINARY)
    centers = []
    for obj in dataLST:#name,(left,right,top,bottum)

        left,right,top,buttom = obj[1]
        left,right,top,buttom = get_new_box(left,right,top,buttom)
        cv2.rectangle(blackAndWhiteImage, (left, top), (right, buttom), (255, 255, 255), -1)
        center = (int((top + buttom)/2),int((left+right)/2))
        centers.append([obj[0],center])
    data = np.asarray( blackAndWhiteImage, dtype="int32" )
    data[data==0] = 1
    data[data==255] = 0
    for center in centers:
        data[center[1]] = 2
    

    path = astar.astar(frame,data,centers[0][1],centers[1][1])
    if (path == False):
        print('sorry, there is no availabale path between those little guys')
        cv2.imshow('.',frame)
    return path

#custom func to get classes name and boxes
def get_boxes(output_dict,category_index,image):
    boxes = output_dict['detection_boxes']
    max_boxes_to_draw = boxes.shape[0]#num of boxes using numpy func
    scores = output_dict['detection_scores']#confident at each object
    min_score_thresh=0.80#minimum confident level
    im_height, im_width = image.shape[:2]
    classes = []
    # iterate over all objects found
    for i in range(max_boxes_to_draw):
        if scores[i] > min_score_thresh:
            ymin, xmin, ymax, xmax = boxes[i]
            (left, right, top, bottom) = (xmin * im_width, xmax * im_width, ymin * im_height, ymax * im_height)
            class_name = category_index[output_dict['detection_classes'][i]]['name']
            cv2.rectangle(image, (int(left), int(top)), (int(right), int(bottom)), (0, 0, 0), 1)
            classes.append([class_name,(int(left),int(right),int(top),int(bottom))])
    return classes

def run_inference_for_single_image(image, graph):      
    image_tensor = tf.get_default_graph().get_tensor_by_name('image_tensor:0')

    # Run inference
    output_dict = sess.run(tensor_dict,
                           feed_dict={image_tensor: np.expand_dims(image, 0)})

    # all outputs are float32 numpy arrays, so convert types as appropriate
    output_dict['num_detections'] = int(output_dict['num_detections'][0])
    output_dict['detection_classes'] = output_dict['detection_classes'][0].astype(np.uint8)
    output_dict['detection_boxes'] = output_dict['detection_boxes'][0]
    output_dict['detection_scores'] = output_dict['detection_scores'][0]
    return output_dict

if(len(sys.argv) == 2):
    #path = '/home/lidorby/Downloads/testing/' +str(1) +'.jpg'
    path = sys.argv[1]
else:
    print('wrong use of arguments, please enter the image path when calling the script')
    sys.exit()


PATH_TO_FROZEN_GRAPH = 'inference_graph/frozen_inference_graph.pb'
PATH_TO_LABELS = 'inference_graph/labelmap.pbtxt'

#load tf to memory
detection_graph = tf.Graph()
with detection_graph.as_default():
  od_graph_def = tf.GraphDef()
  with tf.gfile.GFile(PATH_TO_FROZEN_GRAPH, 'rb') as fid:
    serialized_graph = fid.read()
    od_graph_def.ParseFromString(serialized_graph)
    tf.import_graph_def(od_graph_def, name='')
#load labelmap
category_index = label_map_util.create_category_index_from_labelmap(PATH_TO_LABELS, use_display_name=True)

#load tf
with detection_graph.as_default():
    with tf.Session() as sess:
        ops = tf.get_default_graph().get_operations()
        all_tensor_names = {output.name for op in ops for output in op.outputs}
        tensor_dict = {}
        for key in [
          'num_detections', 'detection_boxes', 'detection_scores',
          'detection_classes', 'detection_masks'
          ]:
            tensor_name = key + ':0'
            if tensor_name in all_tensor_names:
                tensor_dict[key] = tf.get_default_graph().get_tensor_by_name(
              tensor_name)
        #finish loading
        try:
            #clear tab
            os.system('clear') 
            print('starting to check if there is a path')
            #to clac time
            tic = time.perf_counter()
            #open image
            image_np = cv2.imread(path)
            #actual detection withj tf
            output_dict = run_inference_for_single_image(image_np, detection_graph)
            #for threshold and viisualisation
            boxes = get_boxes(output_dict,category_index,image_np)
            #astar
            path = calc_path(image_np,boxes)
            toc = time.perf_counter()
            if path != False:
                print(f"calc the shortest path in {toc - tic:0.4f} seconds")
            else:
                print(f"it took {toc - tic:0.4f} seconds to find out that we cant get those little guys together")

            while cv2.waitKey(0)  != ord('q'):
                pass
            cv2.destroyAllWindows()     
        except Exception as e:
            print(e)
            cv2.destroyAllWindows()

