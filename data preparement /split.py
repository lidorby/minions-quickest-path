import os
import shutil
import random

images_path = 'frames/'
train_path = 'images/train/'
validation_path = 'images/test/'
os.makedirs(train_path)
os.makedirs(validation_path)


for image_file in os.listdir(images_path):
    print(image_file)    
    labels_file = image_file.replace('.jpg', '.xml')
    if random.uniform(0, 1) > 0.2:
        shutil.copy(images_path + image_file, train_path + image_file)
        shutil.copy(images_path + labels_file, train_path + labels_file)
    else:
        shutil.copy(images_path + image_file, validation_path + image_file)
        shutil.copy(images_path + labels_file, validation_path + labels_file)